package com.devcamp.j03_javabasic.s50;

import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Order {
    int id;
    String customerName;
    long price;
    Date orderDate;
    boolean confirm;
    String[] items;

    // Khởi tạo 1 tham số customerName
    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] { "Book", "pen", "ruler" };
    }

    // khởi tạo với
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = true;
        this.items = items;
    }

    // khởi tạo o tham số
    public Order() {
        this("2");
    }

    // khởi tạo với 3 tham số
    public Order(int id, String customerName, long price) {
        this(id, customerName, price, new Date(), true, new String[] { "Trung", "Ca", "Dau", "Rau" });
    }

    @Override
    public String toString() {
        // Định dạng tiêu chuẩn việt nam
        Locale.setDefault(new Locale("vi", "VM"));
        // Định dạng cho ngày thnag
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        // return chuỗi
        return "Order [id=" + id
                + ", customerName=" + customerName
                + ", price=" + usNumberFormat.format(price)
                + ", orderDate="
                + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                + ", confirm: " + confirm
                + ", items=" + Arrays.toString(items);
    }
}
