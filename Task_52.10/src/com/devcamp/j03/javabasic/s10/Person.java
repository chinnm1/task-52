package com.devcamp.j03.javabasic.s10;

public class Person {
    String name; // tên người
    int age;// tuổi
    double weight;// cân nặng
    long salary; // lương
    String[] pets;// vật nuôi
    // khởi tạo một tham số name

    @Override

    public String toString() {
        return "Person [name =" + name + ", age =" + age + "weight=" + weight + "salary=" + salary + "pets=" + pets
                + "]";
    }

    public Person(String name) {
        this.name = name;
        this.age = 18;
        this.weight = 60.5;
        this.salary = 10000000;
        this.pets = new String[] { "Big Dog", "Small Cat", "Gold Fish", "Red Parrot" };

    }

    // Khởi tạo với tất cả tham số
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }
}
